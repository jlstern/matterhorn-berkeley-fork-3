/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.series.canvas;

import org.junit.Test;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.util.env.ApplicationProfile;
import org.opencastproject.util.env.Environment;
import org.opencastproject.util.env.EnvironmentUtil;
import org.osgi.framework.BundleContext;

import java.io.IOException;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
public class LearningToolIntegrationTest {

  private final String seriesId = "8e003255-8131-49b3-9f28-1a5bbb78d145";
  private final String seriesTitle = "Series Title";
  private final String seriesDescription = "Description";
  private final String canvasNavigationLabel = "A label to show in Canvas";
  private final String consumerKey = "someConsumerKeyBlah";
  private final String sharedSecret = "sharedSecret";
  private final DublinCoreCatalogImpl series;
  private final LearningToolIntegration learningToolIntegration;
  private final LTICartridge cartridge;

  public LearningToolIntegrationTest() {
    final BundleContext bundleContext = createMock(BundleContext.class);
    expect(bundleContext.getProperty("edu.berkeley.lti.label")).andReturn(canvasNavigationLabel).once();
    expect(bundleContext.getProperty("edu.berkeley.lti.consumerKey")).andReturn(consumerKey).once();
    expect(bundleContext.getProperty("edu.berkeley.lti.sharedSecret")).andReturn(sharedSecret).once();
    replay(bundleContext);
    learningToolIntegration = LearningToolIntegration.newInstance(bundleContext);
    //
    series = DublinCoreCatalogImpl.newInstance();
    series.set(DublinCore.PROPERTY_IDENTIFIER, seriesId);
    series.set(DublinCore.PROPERTY_TITLE, seriesTitle);
    series.set(DublinCore.PROPERTY_DESCRIPTION, seriesDescription);
    cartridge = learningToolIntegration.getLTICartridge(series);
  }

  @Test
  public void testNewInstance() {
    final String playbackBaseURL = cartridge.getPlaybackBaseURL();
    assertNotNull(playbackBaseURL);
    assertTrue(playbackBaseURL.startsWith("https://"));
    assertNotNull(cartridge.getDomain());
    assertEquals(canvasNavigationLabel, cartridge.getCanvasNavigationLabel());
    assertEquals(consumerKey, cartridge.getConsumerKey());
    assertEquals(seriesDescription, cartridge.getSeriesDescription());
    assertEquals(seriesId, cartridge.getSeriesId());
    assertEquals(seriesTitle, cartridge.getSeriesTitle());
  }

  @Test
  public void testGetBasicLTI() throws IOException {
    final String xml = learningToolIntegration.getLTICartridgeXMLAsString(cartridge);
    assertFalse(xml.contains("{") || xml.contains("}"));
  }

  @Test
  public void testGetSharedSecret() throws IOException {
    assertEquals(sharedSecret, learningToolIntegration.getSharedSecret());
  }

  @Test
  public void testGetBasicLTITolerableNulls() throws IOException {
    series.set(DublinCore.PROPERTY_DESCRIPTION, (String) null);
    final String xml = learningToolIntegration.getLTICartridgeXMLAsString(cartridge);
    assertFalse(xml.contains("{") || xml.contains("}"));
    final String subDomain = Environment.local.equals(EnvironmentUtil.getEnvironment()) ? "localhost"
        : ApplicationProfile.engage.getSubDomain();
    assertTrue(xml.contains(subDomain));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetBasicLTIError() throws IOException {
    series.set(DublinCore.PROPERTY_TITLE, (String) null);
    learningToolIntegration.getLTICartridgeXMLAsString(learningToolIntegration.getLTICartridge(series));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetBasicLTINullPassword() throws IOException {
    series.set(DublinCore.PROPERTY_TITLE, (String) null);
    learningToolIntegration.getLTICartridgeXMLAsString(learningToolIntegration.getLTICartridge(series));
  }

  @Test
  public void testToJSON() throws Exception {
    final String json = cartridge.toJson();
    assertTrue(json.contains(cartridge.getSeriesId()));
    assertTrue(json.contains(cartridge.getSeriesTitle()));
    assertTrue(json.contains(cartridge.getDomain()));
    assertTrue(json.contains(cartridge.getConsumerKey()));
    assertTrue(json.contains(cartridge.getCanvasNavigationLabel()));
  }

}
