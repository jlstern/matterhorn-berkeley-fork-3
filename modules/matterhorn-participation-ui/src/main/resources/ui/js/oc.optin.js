/**
 *  Copyright 2009 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
var ocOptin = (function() {
  var sched = {};
  // REST endpoints
  var SCHEDULER_URL     = '/recordings';
  var NEW_RECORDING_SCHEDULER_URL  = '/admin/index.html#/scheduler';
  var WORKFLOW_URL      = '/workflow';
  var CAPTURE_ADMIN_URL = '/capture-admin';
  var SERIES_URL        = '/series';
  var SERIES_SEARCH_URL = '/series/series.json';
  var RECORDINGS_URL    = '/admin/index.html#/recordings';
  var DUBLIN_CORE_NS_URI  = 'http://purl.org/dc/terms/';
  var ANOYMOUS_URL      = "/info/me.json";
  var DEFAULT_WORKFLOW  = "lecture-process-publish-ucb-standard";
  var COURSES_BASE_URL  = "/courses";
  var APPROVAL_URL      = "/courses/approval";
  // Constants
  var CREATE_MODE       = 1;
  var EDIT_MODE         = 2;
  var SINGLE_EVENT      = 3;
  var MULTIPLE_EVENTS   = 4;
  var SUBMIT_MODE       = 5;
  var START_DELAY	    = 420; // 7 minutes
  var END_DELAY			= 300; // 5 minutes

  sched.mode              = CREATE_MODE;
  sched.type              = MULTIPLE_EVENTS;
  sched.selectedInputs    = '';
  sched.conflictingEvents = false;
  sched.tzDiff            = 0;
  sched.timezone          = '';
  sched.components        = null; //contains components used to make other components like temporal but aren't used in a catalog directly.
  sched.courses			  = [];
  sched.term			  = {};
  sched.user			  = {};

  // Holds value, if any, for a publish delay received from web service
  sched.publishDelayInit;

  // Catalogs
  sched.catalogs = [];
  sched.dublinCore = new ocAdmin.Catalog({ //DC Metadata catalog
    name: 'dublincore',
    serializer: new ocAdmin.DublinCoreSerializer()
  });
  sched.catalogs.push(sched.dublinCore);
  sched.capture = new ocAdmin.Catalog({ //Capture Agent Properties
    name: 'agentparameters',
    serializer: new ocAdmin.Serializer()
  });
  sched.catalogs.push(sched.capture);

  sched.init = function init(){
	//this.internationalize();
	this.getUser();
	this.registerCatalogs();
	this.registerEventHandlers();
    this.showOptionsText();

    var courseOfferingId=ocUtils.getURLParam('courseOfferingId') ? ocUtils.getURLParam('courseOfferingId') : 0 ;
    this.getCourseInfo(courseOfferingId);
    this.checkApprovalStatus(courseOfferingId, sched.user.username);

  };

  sched.checkApprovalStatus = function(courseOfferingId, ldapId) {
	  //ToDo: more possibilities than approved or not here
	  if (false) {
		  $('.hidePostSubmit').hide();
		  $('#previousApproval').show();
	  }
  };

  sched.getCourseInfo = function(courseOfferingId) {
	  $.ajax({
		  type: "GET",
		  dataType: 'json',
		  async: false,
		  url: COURSES_BASE_URL + "/" + courseOfferingId + ".json",
		  success: function(data) {
		    sched.courses=data;
            sched.processCourse(courseOfferingId);
            sched.updateUI();
		  },
		  error: function(data) {
		    switch (data.status) {
                case 403:
                    message = "Your course is now located in a room with no capture capabilities, so you are no longer eligible to sign up for Classroom Capture for the current semester.";
                    ocUtils.log(message);
                    $('#warningMessage').text(message);
                    sched.invalid();
                    break;
                case 404:
                    message = "Our system has no record of the requested course information.";
                    ocUtils.log(message);
                    $('#warningMessage').text(message);
                    sched.invalid();
                    break;
                case 409:
                    message = "Our system information about this room does not match the configuration of the room's capture-agent equipment.";
                    ocUtils.log(message);
                    $('#warningMessage').text(message);
                    sched.invalid();
                    break;
                default:
                    message = "There was an internal system error.";
                    ocUtils.log(message);
                    $('#warningMessage').text(message);
                    sched.invalid();
                    break;
		    }
		  },
		  cache: false
	  });
  }

  sched.invalid = function()  {
	  $('.hideInvalid').hide();
	  $('.showInvalid').show();
	  sched.halt();
  }

  sched.displayError = function()  {
	  $('.showInvalid').show();
	  sched.halt();
  }

  sched.canceled = function() {
	  $('.hideInvalid').hide();
	  $('.projectCanceled').show();
	  sched.halt();
  }

  sched.halt = function() {
	  sched = null;
	  throw "stop execution";
  }

  sched.getUser = function() {
	  $.ajax({
          url: ANOYMOUS_URL,
          type: 'GET',
          dataType: 'json',
          async: false,
          error: function () {
            if (ocUtils !== undefined) {
              ocUtils.log("Could not retrieve user " + ANOYMOUS_URL);
            }
          },
          success: function(data) {
        	sched.user['username'] = data.username;
        	sched.user['roles'] = data.roles;
        	sched.user['isInstructor'] = false;
        	sched.user['isAdmin'] = ( data.roles.lastIndexOf("ROLE_ADMIN") != -1);
          }
      });
  }


  sched.updateUI = function() {
	  $('#coursetitle').text(course.courseTitle);
	  $('#postRecordingTime').html(sched.selectedCourse.meeting);
	  $("label span.unavailable").html("Not available in " + sched.selectedCourse.room.building + ' ' + sched.selectedCourse.room.roomNumber );
	  $('#instructorList').html("Instructors: " + sched.selectedCourse.instructorsParticipation.join(', '));

	  if (sched.selectedCourse.stageOfLifecycle == "cancelled") {
		  sched.canceled();
	  }
	  if ( sched.user.isInstructor ) {

	  } else if ( sched.user.isAdmin ) {
		  $('#showAdminUser').show();
		  //$('#OptIn').val("Schedule recordings");
		  $('#OptIn').removeAttr( 'disabled' ).removeClass('disabled');
		  $('#agreement_confirmation').attr("disabled", true);
	  } else {
		  sched.invalid();
	  }
	  if ( sched.selectedCourse.capturePreferences.custom ) {
		  $('#standardSettings').text("Your co-instructor signed up with the following settings. Change these only if you believe your co-instructor(s) will agree.");
		  $('#settingsReview').text("Review current settings:")
		  $('#revertSettings').html("<a href=''>Undo my changes</a>");
		  sched.showOptionsText();
		  $(".inputSelectedShow").show();
		  $(".inputSelectedHide").hide();
		  
		  $('#revertSettings').hide();
		  $('#standardSettings').show();
		  
	  } else {
		  $(".inputSelectedShow").hide();
	  }
	  if (sched.selectedCourse.scheduled || sched.user['approved']) {
		  if (sched.user['approved']) {
			  $('#SignUpSelection').hide();
			  $('#previousApproval').show();
		  }
		  $('#standardSettings').text("The following settings have been selected for capture, and the recordings have been scheduled. Contact webcast@berkeley.edu if you would like to make any changes to these settings.");
		  $('a.fancybox.change').qtip({
	            content: "Please contact <a href='mailto:webcast@berkeley.edu'>webcast@berkeley.edu</a><br/> to make changes",
	            hide: { fixed: true, delay: 240 },
	            position: { corner: { target: 'topLeft', tooltip: 'topRight' } },
	            style: {
	                name: 'dark', border: 1, cursor: 'pointer',
	                padding: '5px 8px', name: 'blue'
	            }
		  });
		  $('a.fancybox.change').removeClass('fancybox').hide();
	  }
  }

  sched.processCourse = function(courseOfferingId) {
	var coursetitle="";
	var semester;
    var courseTerm;

	sched.selectedCourse=sched.courses;
	course = sched.courses;
	if ( course == undefined) {
		ocUtils.log('could not retrieve course!');
		return false;
	}

    courseTerm = course['term']
    semester = courseTerm['semester'];
    courseTerm['termCode'] = (semester == 'Fall') ? "D" : "B";
	course['series'] = {};
	series = course['series'];
	series['seriesId'] = courseTerm['termYear'] + courseTerm['termCode'] + course.canonicalCourse.ccn;
	series['identifier'] = '';
	series['seriesTitle'] = course.canonicalCourse.title + ", " + course.section + " - " + semester + " " + courseTerm['termYear'];

	//assure course.participations is an array
	course.participationSet = (course.participationSet.constructor == Array)? course.participationSet : [course.participationSet];
	course.multipleInstructors = (course.participationSet.length > 1);
	course['instructorsParticipation'] = [];
	course['instructors'] = [];
	var approvalExists=false;

	for( i in course.participationSet)	{
		var approval = "";
		var p = course.participationSet[i];
		var currentUser = (p.instructor.calNetUID == sched.user.username) ;
		if (currentUser) {
		  approval = "<b>" + p.instructor.firstName + " " + p.instructor.lastName + "</b>";
		  sched.user['isInstructor'] = true;
		  sched.user['isAdmin'] = false;
		  sched.user['approved'] = p.approved;
		  if (p.approved) {
			  approval += " <span id='currentUserSignedUp'>(signed up)</span>";
			  approvalExists = true;
		  } else {
			  approval += " <span id='currentUserSignedUp'>(not yet signed up)</span>";
		  }
		  course.instructorsParticipation.splice( 0, 0, approval );
		} else {
		  approval = p.instructor.firstName + " " + p.instructor.lastName;
		  if (p.approved) {
			  approval += " (signed up)";
			  approvalExists = true;
		  } else {
			  approval += " (not yet signed up)";
		  }
		  course.instructorsParticipation.push( approval );
		}
		course.instructors.push( p.instructor.firstName + " " + p.instructor.lastName );
	}

	//only set defaults if anyone has approved
	if (approvalExists) {
		sched.setDefaults(course.capturePreferences);
	}

	// ===================================================================================================
	  // Populate local value for publish delay from that returned by web service
	  if ( course.capturePreferences.delayPublishByDays != null && course.capturePreferences.delayPublishByDays != "0" ) {
		  sched.publishDelayInit = course.capturePreferences.delayPublishByDays;
	  } else {
		  sched.publishDelayInit = 0;
	  }
	  ocUtils.log("Setting publishDelayInit to " + sched.publishDelayInit );
	  // ===================================================================================================

	course.meeting = course['meetingDays'].join(', ');
	series['seriesDescr'] = course['description'] || course.canonicalCourse.title + " " + courseTerm['termYear'] + courseTerm['termCode'] + " - " + course.instructors.join(', ');
	series['courseTitle'] = course.canonicalCourse.title + " " + semester + " " + courseTerm['termYear'] + "--" + course.room.building + " " +
	                        course.room.roomNumber + course.meeting + " " + courseTerm['startTime'] + "-" + course['endTime'] ;
	course['courseTitle'] = course.canonicalCourse.title + " | " + course.room.building + " " +course.room.roomNumber + " | " +
                            course.meeting + " " + getFormattedTime(course['startTime']) + "-" + getFormattedTime(course['endTime']) ;

	course['days'] = {};
	course.days={'SU':false, 'MO':false, 'TU':false, 'WE':false, 'TH':false, 'FR':false, 'SA':false};

	var days = course.meetingDays;
	for (var i =0;  i< days.length ; i++) {
		var day = days[i].substring(0,2).toUpperCase();
		course.days[day] = (days[i] != ' ') ? true : false ;
	}

	course.meeting += " " + formatTimeWithDelay( course['startTime'] , START_DELAY ) + " to " + formatTimeWithDelay( course['endTime'] , END_DELAY );
	course['agentName'] = (course.room.building + '-' + course.room.roomNumber).toLowerCase();

	// ToDo: the agent's time zone needs to come from somewhere real!
	// var agentTz=-420
	// sched.dublinCore.components.agentTimeZone.setValue(agentTz);
	// sched.handleAgentTZ(agentTz);
	var roomCapability = course.room.capability;
	sched['agentCapabilities'] = roomCapability;
	if (roomCapability == 'screencast') {
	    sched.agentDevices = { microphone: 'audio', screen: 'screen' }
	} else if (roomCapability == 'screencastAndVideo') {
	    sched.agentDevices = { microphone: 'audio', screen: 'screen', presenter: 'video' }
	}
	sched.handleAgentDevices();

	// Get term date range
	var term={};
	if ( courseTerm.semesterStartDate != null && courseTerm.semesterEndDate != null ) {
		term['instruction_begin']= courseTerm.semesterStartDate;
		term['instruction_end'] = courseTerm.semesterEndDate;
		sched.term=term;
	} else {
		message = "Your course is missing course start and / or course end dates.";
        ocUtils.log(message);
        $('#warningMessage').text(message);
        sched.invalid();
	}

  };
  function formatTimeWithDelay(time, delay) {
	  var d = new Date();
	  hour = parseInt(time.substr(0,2),10);
	  minute = parseInt(time.substr(2,2),10);
	  d.setHours(hour);
	  d.setMilliseconds(0);
	  d.setSeconds(0);
	  d.setMinutes(minute);
	  d = d.valueOf();
	  d += delay * 1000;
	  d = new Date(d);
	  var militaryTime = d.getHours() + ((d.getMinutes() < 10) ? "0" : "") + d.getMinutes();
	  return getFormattedTime(militaryTime)
  }

  sched.setDefaults = function(prefs) {
	  var customPrefs = false;

	  if ( prefs.recordingAvailability != null && prefs.recordingAvailability != "publicCreativeCommons" ) {
		  sched.setDefault( 'recordingAvailability', prefs.recordingAvailability );
		  customPrefs = true;
	  }
	  if ( prefs.recordingType != null && prefs.recordingType != "none" ) {
		  $('#selectable li').removeClass("ui-selected default").addClass("custom");
		  $('#selectable #'+ prefs.recordingType).addClass("ui-selected default").removeClass("custom").attr("checked","checked");

		  show = $('#selectable .ui-selected')[0].getAttribute('show');
		  $('#inputSelectionAsText .optionsDescription').hide()
		  $('#inputSelectionAsText .' + show).show();
		  customPrefs = true;
	  }

	  prefs.custom = customPrefs;
  }

  sched.setDefault = function(input,value) {
	  $('input[name=' + input + ']').addClass("custom").removeClass("chosen default").removeAttr("checked");
	  $('input[name=' + input + '][show=' + value + ']').removeClass("custom").addClass("chosen default").attr("checked","checked")
  }

  sched.handleAgentDevices = function() {
	  var devices = sched.agentDevices;
	  var eligibility = ["Audio Only (no cost)" , "Computer Screen with Audio (no cost)" , "Video with Audio", "Computer Screen Video & Audio"];
	  var isScreenAvailable = devices != undefined && devices.screen != undefined && devices.screen != '';
	  var isPresenterAvailable = devices != undefined && devices.presenter != undefined && devices.presenter != '';

	  if (isScreenAvailable || isPresenterAvailable) {
        document.getElementById('audioOnly').setAttribute('value', devices.microphone );
	  }

	  if (isScreenAvailable) {
	    document.getElementById('screencast').setAttribute('value', devices.microphone + ',' + devices.screen );
	    if (isPresenterAvailable) {
	    	document.getElementById('videoAndScreencast').setAttribute('value', devices.microphone + ',' + devices.screen + ',' + devices.presenter);
	    	document.getElementById('videoOnly').setAttribute('value', devices.microphone + ',' + devices.presenter );
	    }
	  } else if (isPresenterAvailable) {
		  document.getElementById('videoOnly').setAttribute('value', devices.microphone + ',' + devices.presenter );
	  }
	  if (!isScreenAvailable) {
		  $( "#selectable li.screen" ).selectable({ filter: 'li', disabled: true });
		  eligibility[1] = ''; eligibility[3] = '';
	  };
	  if (!isPresenterAvailable) {
		  $( "#selectable li.video" ).selectable({ filter: 'li', disabled: true });
		  eligibility[3] = ''; eligibility[2] = '';
	  };
	  //remove empty options:
	  eligibility = $.grep(eligibility,function(n){ return(n); });
	  $('#eligibletext').text(eligibility.join(' | '));
  }

  sched.setDateRange = function(term) {
	sched.term = term;
	ocUtils.log("Set start date to " + term.instruction_begin + " and end date to " + term.instruction_end);
  };

  sched.internationalize = function internationalize() {
    //Do internationalization of text
    jQuery.i18n.properties({
      name:'scheduler',
      path:'i18n/'
    });
    ocUtils.internationalize(i18n, 'i18n');
    //Handle special cases like the window title.
    document.title = i18n.window.schedule + " " + i18n.window.prefix;
    $('#i18n_page_title').text(i18n.page.title.sched);
  };

  sched.registerEventHandlers = function registrerEventHandlers(){
    var initializerDate;
    initializerDate = new Date();
    initializerDate.setHours(initializerDate.getHours() + 1); //increment an hour.
    initializerDate.setMinutes(0);

    //UI Functional elements
    $('.oc-ui-collapsible-widget .form-box-head').click(
    function() {
      $(this).children('.ui-icon').toggleClass('ui-icon-triangle-1-e');
      $(this).children('.ui-icon').toggleClass('ui-icon-triangle-1-s');
      $(this).next().toggle();
      return false;
    });

    $('.fancybox').fancybox({
    	'padding' : 10,
    	'closeBtn': true,
    	'onClosed'		: function() {
    		$('input.chosen').prop("checked", true);
        	$.fancybox.close();
    	}
    });
    $('.cancelLightBox').bind("click", jQuery.fancybox.close );

    $('#inputSelectionSubmit .confirmChange').click( function() {
    	$.fancybox.close();
    	sched.showOptionsText();
    });
    $('#inputSelectionSubmit .cancelChange').click( function() {
    	$('#selectable li.default').addClass("ui-selected").siblings().removeClass("ui-selected");
    	$.fancybox.close();
    	sched.showOptionsText();
    });

    $('.confirmChange').click( function() {
    	$('input:radio').removeClass('chosen');
    	$('input:radio:checked').addClass('chosen');
    	$.fancybox.close();
    	sched.showOptionsText();
    });

    $('.cancelChange').click( function() {
    	$('input.chosen').prop("checked", true);
    	$.fancybox.close();
    });

    $('#revertSettings').click( function() {
    	$('input.default').prop("checked", true);
    	$('#selectable li.default').addClass("ui-selected").siblings().removeClass("ui-selected");
    	$('.confirmChange').click();
    	sched.showOptionsText();
    	// Do a reload page here
    	location.reload();
    	return false;
    })

    $('#OptIn').click(this.submitForm);

    $('#agreement_confirmation').click( function(){
    	var isChecked = $('#agreement_confirmation').attr('checked')?true:false;
    	if (isChecked) {
    		$('#OptIn').removeAttr( 'disabled' ).removeClass('disabled');
    	} else {
    		$('#OptIn').attr('disabled', 'disabled').addClass('disabled');
    	}
    });

    $("a.toggle").click(function(event){
        var href = $(this).attr("href");
        $(href).toggle('fast');
        $('#moretext, #lesstext').toggle();
  	    event.preventDefault();
  	  });
    $("#selectable").selectable({
      filter: 'li',
      selected: function(event, ui) {
    	$('#postRecordingType').text( $('#selectable .ui-selected #selectable-label').text());
        $(ui.selected).siblings().removeClass("ui-selected");
      }
    });

    $('#cancelButton').click(this.cancelForm);

    //multiple recording specific elements
    $('#recurStart').datepicker({
      showOn: 'both',
      buttonImage: '/optin/img/icons/calendar.gif',
      buttonImageOnly: true,
      dateFormat: 'yy-mm-dd'
    });

    $('#recurEnd').datepicker({
      showOn: 'both',
      buttonImage: '/optin/img/icons/calendar.gif',
      buttonImageOnly: true,
      dateFormat: 'yy-mm-dd'
    });

    $('input#title,span.scheduler-instruction-text').hover(function(){
      $('li#titleNote span.scheduler-instruction-text').addClass('scheduler-instruction-text-hover');
    }, function(){
      $('li#titleNote span.scheduler-instruction-text').removeClass('scheduler-instruction-text-hover');
    });

    //Publish delay option toggle buttons
    $('#delayPublishByDaysZero').click( function() {
  	  ocUtils.log('delayPublishByDaysZero  clicked');
  	  $('#delayPublishByDaysDays').val('0');
  	  sched.publishDelayInit = 0;
  	  });

    $('#delayPublishByDaysMany').click( function() {
  	  ocUtils.log('delayPublishByDaysMany  clicked');
      $('#delayPublishByDaysDays').val('1');
  	  //$('#delayPublishByDaysDays').attr('disabled', false);
  	  //$('#delayPublishByDaysDays').removeAttr( 'disabled' ).removeClass('disabled');
  	  });

    $('#delayPublishByDaysDays').change( function() {
      ocUtils.log('delayPublishByDaysDays  change');
      if ($('#delayPublishByDaysDays').val() > 0) {
    	  $('#delayPublishByDaysMany').attr('checked', true);
    	  sched.publishDelayInit = $('#delayPublishByDaysDays').val();
      }
      });
  }

  sched.showOptionsText = function showOptionsText() {
	var recordingAvailability=$('input[name=recordingAvailability]:checked').val();
  	var recordType = $('#selectable .ui-selected')[0].getAttribute('show');

  	$('.optionsDescription').hide();
  	$('.optionsDescription.' + recordingAvailability).show();
  	$('.optionsDescription.' + recordType).show();

  	// ===================================================================================================
  	$('.optionsDescription.' + delayPublishByDays).show();
  	var delayPublishByDays=$('input[name=delayPublishByDays]:checked').attr('show');

	if (sched.publishDelayInit > 0) {
		// A non-zero value for publish delay received from web service
		ocUtils.log('A non-zero value for publish delay received from web service = ' + sched.publishDelayInit);
		$('#delayPublishByDays').text(sched.publishDelayInit);
		$('#delayPublishByDaysDays').val(sched.publishDelayInit);
		$('#delayPublishByDaysMany').attr('checked', true);
		$('#delayPublishByDaysDefault').hide();
		$('.delayPublishByDaysRead').show();
	} else {
  		ocUtils.log('Should have no choices made');
  		$('.changedMarker.delayPublishByDays').hide();
        $('#delayPublishByDaysDays').val('0');
  		$('#delayPublishByDays').text(0);
  		$('#delayPublishByDaysDefault').show();
  		$('.delayPublishByDaysRead').hide();
  		$('#revertSettings').hide();
		$('#standardSettings').show();
  	}

  	if ($('input[name=delayPublishByDays].custom.chosen').length == 1 ) {
  		ocUtils.log('Options edited');
  		$('#delayPublishByDays').text($('#delayPublishByDaysDays').val());
  		$('.delayPublishByDaysRead').show();
  		$('.changedMarker.delayPublishByDays').show();
  	}
  	// ===================================================================================================

  	if ($('input[name=recordingAvailability].custom.chosen').length == 1 ) {
  		$('.changedMarker.recordingAvailability').show();
  	} else {
  		$('.changedMarker.recordingAvailability').hide();
  	}

  	if ($('#selectable li.custom.ui-selected').length == 1 ) {
  		$('.changedMarker.recordType').show();
  	} else {
  		$('.changedMarker.recordType').hide();
  	}

  	$('#postrecordingAvailability').html( $('.optionsDescription.' + recordingAvailability).html() );
  	
  	if ( ($('input.custom:checked').length != 0) || $('#selectable li.custom.ui-selected').length != 0) {
		$('#revertSettings').show();
		$('#standardSettings').hide();
	} else {
		$('#revertSettings').hide();
		$('#standardSettings').show();
	}
  	
  	// ===================================================================================================
  	if ($('#delayPublishByDaysDays').val() > 0) {
  		var postDelayPublishByDays = $('#delayPublishByDaysDays').val() + " days after capture";
  		$('#postDelayPublishByDays').html(postDelayPublishByDays);
  	} else {
  		$('#postDelayPublishByDays').html( $('.optionsDescription.' + delayPublishByDays).html() );
  	}
  	// ===================================================================================================
  }

  sched.submitForm = function(){
    var payload = {};
    var error = false;


	hideUserMessages();
    sched.checkForConflictingEvents();
    if(ocOptin.conflictingEvents) {
      $('#missingFieldsContainer').show();
      $('#errorConflict').show();
      $('#errorConflict li').show();
      return false;
    }

    $.extend(true, sched.capture.components, ocOptin.workflowComponents);

    var errors = [];
    for (var i in sched.catalogs) {
      var serializedCatalog = sched.catalogs[i].serialize();
      if (!serializedCatalog) {
        errors = errors.concat(sched.catalogs[i].getErrors());
      } else {
        payload[sched.catalogs[i].name] = serializedCatalog;
      }
    }

    if(errors.length > 0) {
      showUserMessages(errors);
    } else {
      $('#OptIn').attr('disabled', 'disabled').addClass('disabled');
      if(sched.type !== SINGLE_EVENT)
      {
        $('#submitModal').dialog(
        {
          modal: true,
          resizable: false,
          draggable: false,
          create: function (event, ui)
          {
            $('.ui-dialog-titlebar-close').hide();
          }
        });
      }

      var approval = {};
      var dcIndex = 1;
      if (sched.catalogs[0].name == "dublincore") { dcIndex = 0; }
      approval['recordingType'] = ["audioOnly","screencast","videoOnly","videoAndScreencast"][$('#selectable .ui-selected').index()];
      approval['instructorLdapId'] = sched.user.username;
      approval['courseOfferingId'] = sched.selectedCourse.courseOfferingId;
      approval['approved'] = !sched.user.isAdmin;
      approval['delayPublishByDays'] = $('#delayPublishByDaysDays').val();
      approval['recordingAvailability'] = $(".recordingAvailability:checked").val();

      ocUtils.log("Approved:");
      ocUtils.log(" instructorLdapId: " + approval['instructorLdapId']);
      ocUtils.log(" recordingType: " + approval['recordingType']);
      ocUtils.log(" approved: " + approval['approved']);
      ocUtils.log(" delayPublishByDays: " + approval['delayPublishByDays']);
      ocUtils.log(" recordingAvailability: " + approval['recordingAvailability']);


      $.ajax({
    	  type: 'POST',
    	  url: APPROVAL_URL + '/' + course.courseOfferingId,
    	  data: approval,
    	  async: false,
    	  dataType: 'json',
    	  success: function(data) {
    		  sched.selectedCourse['isReadyToSchedule']  = data.isReadyToSchedule;
    		  if ( !sched.selectedCourse.scheduled && sched.selectedCourse.isReadyToSchedule ) {
                  payload['courseOfferingId'] = sched.selectedCourse.courseOfferingId;
                  // TODO: display a processing message here
    	    	  $.ajax({
    	    		  type: 'POST',
    	    		  url: SCHEDULER_URL + '/',
    	    		  data: payload,
    	    		  dataType: 'json'
    	    	  });
    	      }
			  sched.eventSubmitComplete();
    	  },
    	  error: function (xhr, ajaxOptions, thrownError) {
    		  ocUtils.log("Could not post approval " + approval['courseOfferingId']);
    		  ocUtils.log(xhr.status);
    		  ocUtils.log(thrownError);
    	  }
      });
    }
    return true;
  };

  sched.cancelForm = function() {
    document.location = RECORDINGS_URL + "?" + window.location.hash.split('?')[1];
  };


  sched.handleAgentChange = function(elm){
    var time;
    var agent = elm.target.value;
    $('#noticeContainer').hide();
    $(ocOptin.inputList).empty();
    sched.dublinCore.components.agentTimeZone = new ocAdmin.Component(['agentTimeZone'],
    {
      key: 'agentTimeZone',
      nsPrefix: 'oc',
      nsURI: 'http://www.opencastproject.org/matterhorn/',
    });
    if(agent){
      $.get('/capture-admin/agents/' + agent + '/configuration.xml',
      function(doc){
        var devNames = [];
        var capabilities = [];
        $.each($('item', doc), function(a, i){
          var s = $(i).attr('key');
          if(s === 'capture.device.names'){
            devNames = $(i).text().split(',');
          } else if(s.indexOf('.src') != -1) {
            var name = s.split('.');
            capabilities.push(name[2]);
          } else if(s == 'capture.device.timezone.offset') {
            var agentTz = parseInt($(i).text());
            if(agentTz !== 'NaN'){
              sched.dublinCore.components.agentTimeZone.setValue(agentTz);
              sched.handleAgentTZ(agentTz);
            }else{
              ocUtils.log("Couldn't parse TZ");
            }
          } else if(s == 'capture.device.timezone') {
            sched.timezone = $(i).text();
          }
        });
        if(devNames.length > 0) {
          capabilities = devNames;
        }
        if(capabilities.length){
          sched.displayCapabilities(capabilities);
        }else{
          sched.tzDiff = 0; //No agent timezone could be found, assume local time.
          $('#inputList').html('Agent defaults will be used.');
          delete sched.dublinCore.components.agentTimeZone;
        }
        sched.checkForConflictingEvents();
      });
    } else {
      // no valid agent, change time to local form what ever it was before.
      delete sched.dublinCore.components.agentTimeZone; //Being empty will end up defaulting to the server's Timezone.
      if(sched.type === SINGLE_EVENT){
        time = sched.components.startDate.getValue();
      }else if(sched.type === MULTIPLE_EVENTS){
        time = sched.components.recurrenceStart.getValue();
      }
    };
  }

  sched.displayCapabilities = function(capabilities) {
    $(this.inputList).append('<ul class="oc-ui-checkbox-list">');
    $.each(capabilities, function(i, v) {
      $(sched.inputList).append('<li><input type="checkbox" id="' + v + '" value="' + v + '">&nbsp;<label for="' + v +'">' + v.charAt(0).toUpperCase() + v.slice(1).toLowerCase() + '</label></li>');
    });
    if(this.mode === CREATE_MODE) {
      $(':input', this.inputList).attr('checked', 'checked');
    }
    $(this.inputList).append('</ul>');
    this.capture.components.resources.setFields(capabilities);
    if(ocUtils.getURLParam('edit')) {
      this.capture.components.resources.setValue(sched.selectedInputs);
    }
    // Validate if an input was chosen
    this.inputCount = $(this.inputList).children('input:checkbox').size();
    total = this.inputCount;
    $(this.inputList).each(function() {
      $(this).children('input:checkbox').click(function() {
        total = (this.checked) ? (total = (total < 3) ? total+=1 : total) : total-=1;
        if(total < 1) {
          var position = $('#help_input').position();
          $('#inputhelpBox').css('left',position.left + 100 +'px');
          $('#inputhelpBox').css('top',position.top);
          $('#inputhelpTitle').text('Please Note');
          $('#inputhelpText').text('You have to select at least one input in order to schedule a recording.');
          $('#inputhelpBox').show();
        }
        else {
          $('#inputhelpBox').hide();
        }
      });
    });
  };

  sched.handleAgentTZ = function(tz) {
    var agentLocalTime = null;
    var localTZ = -(new Date()).getTimezoneOffset(); //offsets in minutes
    sched.tzDiff = 0;
    if(tz != localTZ){
      //Display note of agent TZ difference, all times local to capture agent.
      //update time picker to agent time
      sched.tzDiff = tz - localTZ;
      if(this.type == SINGLE_EVENT) {
        agentLocalTime = this.components.startDate.getValue() + (sched.tzDiff * 60 * 1000);
        this.components.startDate.setValue(agentLocalTime);
      }else if(this.type == MULTIPLE_EVENTS){
        agentLocalTime = this.components.recurrenceStart.getValue() + (sched.tzDiff * 60 * 1000);
        this.components.recurrenceStart.setValue(agentLocalTime);
      }
      diff = Math.round((sched.tzDiff/60)*100)/100;
      if(diff < 0) {
        postfix = " hours earlier";
      }else if(diff > 0) {
        postfix = " hours later";
      }
      //$('#noticeContainer').show();
      //$('#noticeTzDiff').show();
      //$('#tzdiff').replaceWith(Math.abs(diff) + postfix);
    }
  };

  sched.checkAgentStatus = function(doc) {
    var state = $('state', doc).text();
    if(state == '' || state == 'unknown' || state == 'offline') {
      $('#noticeContainer').show();
      $('#noticeOffline').show();
    }
  };

  /**
   *  loadKnownAgents calls the capture-admin service to get a list of known agents.
   *  Calls handleAgentList to populate the dropdown.
   */
  sched.loadKnownAgents = function() {
    $(this.agentList).empty();
    $(this.agentList).append($('<option></option>').val('').html('Choose one:'));
    $.get(CAPTURE_ADMIN_URL + '/agents.json', this.handleAgentList, 'json');
  };

  /**
   *  Popluates dropdown with known agents
   *
   *  @param {XML Document}
   */
  sched.handleAgentList = function(data) {
	var agents = ocUtils.ensureArray(data.agents.agent);
	$.each(agents, function(i, agent) {
		$(sched.agentList).append($('<option></option>').val(agent.name).html(agent.name));
	});
    sched.loadEvent();
  };

  sched.loadEvent = function(){
    var eventId = ocUtils.getURLParam('eventId');
    if(eventId && ocUtils.getURLParam('edit')) {
      $.ajax({
        type: "GET",
        url: SCHEDULER_URL + '/' + eventId + '.json',
        success: function(data) {
          sched.dublinCore.deserialize(data);
          sched.checkForConflictingEvents();
        },
        cache: false
      });
      $.ajax({
        type: "GET",
        url: SCHEDULER_URL + '/' + eventId + '/agent.properties',
        success: function(data) {
          sched.capture.deserialize(data);
        },
        cache: false
      });
    }
  }

  sched.eventSubmitComplete = function() {
	  var approvalMsg = "";
	  var videoMsg = "Please note that we must have a chartstring from your department MSO prior to the start of the semester, " +
	  "since a recording type that includes video has been requested. (The cost for the semester is $2000.)";
	  $('.hidePostSubmit').hide();
	  $('.showPostSubmit').show();
	  $('#currentUserSignedUp').text("(signed up)");

	  //if video capture:
	  if ($('#selectable .ui-selected')[0].getAttribute('value').indexOf("presenter") > 0) {
		  $('#videoCostMsg').text(videoMsg).show();
	  }

	  if ( sched.user.isAdmin ) {
		  approvalMsg = "Recordings will be scheduled";
	  } else {
		  if (sched.selectedCourse.multipleInstructors) {
			  if (sched.selectedCourse.isReadyToSchedule) {
				  //everyone approved, we're scheduling
				  approvalMsg = "All Instructors have now signed up; recordings will be scheduled";
			  } else {
				  //awaiting additional approval
				  approvalMsg = "Recordings will not be scheduled, however, until all instructors have signed up.";
			  }
		  } else {
			  if (sched.selectedCourse.isReadyToSchedule) {
				  //single approved, we're scheduling
				  approvalMsg = "Recordings will be scheduled";
			  } else {
				  //ERROR CASE
			  }
		  }
	  }
	  $('#approvalMsg').text(approvalMsg).show();
	  $('#submit_success').css("display","inline");
  }

  sched.checkForConflictingEvents = function() {
    var start, end;
    var data = {
      device: '',
      start: 0,
      end: 0
    };
    sched.conflictingEvents = false;

    return false;

    $('#missingFieldsContainer').hide();
    $('#missingFieldsContainer li').hide();
    $('#errorConflict').hide();
    $('#conflictingEvents').empty();
    if(sched.dublinCore.components.device.validate().length === 0) {
      data.device = sched.dublinCore.components.device.getValue()
    }else{
      return false;
    }

    if(sched.components.recurrenceStart.validate().length === 0 && sched.components.recurrenceEnd.validate().length === 0 &&
    		sched.dublinCore.components.recurrence.validate().length === 0 && sched.components.recurrenceDuration.validate().length === 0){
    	data.start = sched.components.recurrenceStart.getValue();
    	data.end = sched.components.recurrenceEnd.getValue();
    	data.duration = sched.components.recurrenceDuration.getValue();
    	data.rrule = sched.dublinCore.components.recurrence.getValue();
    	if(sched.timezone == ''){
    		return false;
    	}
    	data.timezone = sched.timezone;
    } else {
    	return false;
    }

    $.ajax({
      url: SCHEDULER_URL + "/conflicts.json",
      async: false,
      data: data,
      type: 'get',
      success: function(data) {
        var events = [];
        data = data.catalogs
        if (data != '') {
          for (var i in data) {
            var event = data[i];
            curId = $('#eventId').val();
            eid = ocUtils.getDCJSONParam(event, 'identifier');
            if(sched.mode === CREATE_MODE || (sched.mode === EDIT_MODE && curId !== eid)) {
              sched.conflictingEvents = true;
              $('#conflictingEvents').append('<li><a href="index.html#/scheduler?eventId=' + eid + '&edit=true" target="_new">' + ocUtils.getDCJSONParam(event, 'title') + '</a></li>');
            }
          }
          if(sched.conflictingEvents) {
            $('#missingFieldsContainer').show();
            $('#errorConflict').show();
          }
        }
      }});
  }

  sched.registerCatalogs = function registerCatalogs() {
    var compositeComps = {}; //These components are used to make composite components like temporal (start, end, duration)
    var dcComps = {}; //Dublin Core components
    var agentComps = {}; //Capture agent parameters

	dcComps.title = new ocAdmin.Component([], {
        key: 'title',
        required: false,
        errors: {
          missingRequired: new ocAdmin.Error('missingTitle', 'titleLabel')
        }
      }, {
    	getValue: function() { return sched.selectedCourse.canonicalCourse.title;},
        setValue: function(value) { }
      });

    dcComps.creator = new ocAdmin.Component([], {
      key: 'creator', //presenter
      required: false

    }, {
    	getValue: function() { return sched.selectedCourse.instructors.join(', ');},
        setValue: function(value) { }
      });

    dcComps.contributor = new ocAdmin.Component([], {
      key: 'contributor' // department / sponsor
    });

    //dcComps.delayPublishByDays = new ocAdmin.Component(['delayPublishByDays_0','delayPublishByDays_1'],
      // {label: 'delayPublishByDaysLabel', key: 'delayPublishByDays', required: true,
    	//  errors: {
          //  missingRequired: new ocAdmin.Error('missingdelayPublishByDays', 'delayPublishByDaysLabel')
         // }
       //},
       //{
         //getValue: function() {
        	// return $(".delayPublishByDays:checked").val();},
         //setValue: function(value) { }
       //}
     //);

    dcComps.seriesId = new ocAdmin.Component(['series', 'seriesSelect'],
    {
      required: false,
      key: 'isPartOf',
      errors: {
        missingRequired: new ocAdmin.Error('missingSeries', 'seriesLabel'),
        seriesError: new ocAdmin.Error('errorSeries')
      }
    },
    {
      getValue: function() {
        if(sched.selectedCourse.series.seriesId) {
          this.value = sched.selectedCourse.series.seriesId;
        }
        return this.value;
      },
      setValue: function(value) {
    	sched.selectedCourse.series.seriesId= value;
      },
      asString: function() {
        if(sched.selectedCourse.series) {
          return sched.selectedCourse.series.seriesName;
        }
        return this.getValue() + '';
      },
      validate: function() {
        var error = [];
        series = sched.selectedCourse.series;
        if(series.seriesId !== '' && series.identifier === '') { //have text and no id
          if(!this.createSeriesFromSearchText()) {
            error.push(this.errors.seriesError); //failed to create series for some reason.
          }
        }
        if(series.identifier === '' && this.required) {
          error.push(this.errors.missingRequired);
        }
        return error;
      },
      toNode: function(parent) {
        if(parent) {
          doc = parent.ownerDocument;
        } else {
          doc = document;
        }
        if(this.getValue() != "" && this.asString() != "") { //only add series if we have both id and name.
          seriesId = doc.createElement(this.key[0]);
          seriesId.appendChild(doc.createTextNode(this.getValue()));
          seriesName = doc.createElement(this.key[1]);
          seriesName.appendChild(doc.createTextNode(this.asString()));
          if(parent && parent.nodeType){
            parent.appendChild(seriesId);
            parent.appendChild(seriesName);
          }else{
            ocUtils.log('Unable to append node to document. ', parent, seriesId, seriesName);
          }
        }
      },
      createSeriesFromSearchText: function(){
        var seriesDc, seriesComponent, seriesId;
        var creationSucceeded = false;
        var series = sched.selectedCourse.series;

        if(series.seriesId !== ''){
            seriesDc = '<dublincore xmlns="http://www.opencastproject.org/xsd/1.0/dublincore/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:oc="http://www.opencastproject.org/matterhorn"><dcterms:title xmlns="">' +
                    series.seriesTitle + '</dcterms:title><dcterms:identifier>' +
                    series.seriesId + '</dcterms:identifier><dcterms:contributor>' +
                    sched.selectedCourse.instructors.join(', ') + '</dcterms:contributor><dcterms:description xml:lang="en">' +
                    series.seriesDescr + '</dcterms:description></dublincore>';
          seriesComponent = this;

          var anonymous_role = 'anonymous';
          $.ajax({
              url: ANOYMOUS_URL,
              type: 'GET',
              dataType: 'json',
              async: false,
              error: function () {
                if (ocUtils !== undefined) {
                  ocUtils.log("Could not retrieve anonymous role " + ANOYMOUS_URL);
                }
              },
              success: function(data) {
              	anonymous_role = data.org.anonymousRole;
              }
          });
          $.ajax({
            async: false,
            type: 'POST',
            url: SERIES_URL + '/',
            data: {
              series: seriesDc,
              acl: '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><acl xmlns="http://org.opencastproject.security"><ace><role>' + anonymous_role + '</role><action>read</action><allow>true</allow></ace></acl>'
            },
            dataType: 'xml',
            success: function(data){
              window.debug = data;
              creationSucceeded = true;
              seriesId = $(data).find('dcterms\\:identifier').text();  // <= this isn't working, so hacking with the following line
              seriesId = series.seriesId;

              ocUtils.log('created series: <a href="http://localhost:8080/admin/index.html#/viewseries?seriesId=' + seriesId + '">' + seriesId +'</a>');
              //$('#series').val(seriesId);
              series.identifier = seriesId;
            },
            error: function() {
              creationSucceeded = false;
              ocUtils.log("Error creating series");
              sched.displayError();
            }
          });
        }
        return creationSucceeded;
      }
    });

    agentComps.resources = new ocAdmin.Component([],
    {
      key: 'capture.device.names',
      required: true,
      errors: {
        missingRequired: new ocAdmin.Error('missingInputs', 'inputLabel')
      }
    },
    {
      getValue: function() {
    	//return $('.devices:checked').val();
    	return $('#selectable .ui-selected')[0].getAttribute('value')
      },
      setValue: function(value) { },
      validate: function() {
        if(!this.required || this.getValue() != "") {
          return []; //empty error array.
        };
        return this.errors.missingRequired;
      }
    });

    compositeComps.recurrenceStart = new ocAdmin.Component([],
      {
        required: true,
        key: 'startDate'
      },
      {
        getValue: function() {
          var date, start, hour, minute, a, time;
          date = $.datepicker.parseDate('yy-mm-dd',ocOptin.term.instruction_begin);
          if(date && date.constructor == Date) {
            start = date / 1000; // Get date in milliseconds, convert to seconds.

            time = ocOptin.selectedCourse.startTime;
            hour = parseInt(time.substr(0,2),10);
            minute = parseInt(time.substr(2,2),10);

            start += hour * 3600; // convert hour to seconds, add to date.
            start += minute * 60; //convert minutes to seconds, add to date.
            start += START_DELAY;
            start -= sched.tzDiff * 60; //Agent TZ offset
            start = start * 1000; //back to milliseconds
            return start;
          }
        },
        setValue: function(value) {},
        validate: function() {
          return [];
        },
        asString: function() {
          return (new Date(this.getValue())).toLocaleString();
        }
      });

    compositeComps.recurrenceDuration = new ocAdmin.Component([],
      {
        required: true,
        key: 'duration',
        errors: {
          missingRequired: new ocAdmin.Error('missingDuration', 'recurDurationLabel')
        }
      },
      {
        getValue: function() {
          var duration = 0;

          if(this.validate()) {
        	time   = ocOptin.selectedCourse.endTime;
        	duration += parseInt(time.substr(0,2),10) * 3600;
            duration += parseInt(time.substr(2,2),10) * 60;

            time = ocOptin.selectedCourse.startTime;
            duration -= parseInt(time.substr(0,2),10) * 3600;
            duration -= parseInt(time.substr(2,2),10) * 60;

            duration -= START_DELAY;
            duration += END_DELAY;
        	this.value = (duration * 1000);
          }
          return this.value;
        },
        setValue: function(value) {},
        validate: function() {
          if (ocOptin.selectedCourse.endTime.length == 4 && ocOptin.selectedCourse.startTime.length == 4) {
        	  return [];
          }
        },
        asString: function() {
          var dur = this.getValue() / 1000;
          var hours = Math.floor(dur / 3600);
          var min   = Math.floor( ( dur /60 ) % 60 );
          return hours + ' hours, ' + min + ' minutes';
        }
      });

    compositeComps.recurrenceEnd = new ocAdmin.Component([],
      {
        required: true,
        key: 'endDate',
        errors: {
          missingRequired: new ocAdmin.Error('errorRecurStartEnd', 'recurEndLabel')
        }
      },
      {
        getValue: function() {
        	var date, end, hour, minute, a, time;
            date = $.datepicker.parseDate('yy-mm-dd',ocOptin.term.instruction_end);
            if(date && date.constructor == Date) {
              end = date / 1000; // Get date in milliseconds, convert to seconds.

              time = ocOptin.selectedCourse.endTime;
              hour = parseInt(time.substr(0,2),10);
              minute = parseInt(time.substr(2,2),10);

              end += hour * 3600; // convert hour to seconds, add to date.
              end += minute * 60; //convert minutes to seconds, add to date.
              end += END_DELAY;
              end -= sched.tzDiff * 60; //Agent TZ offset
              end = end * 1000; //back to milliseconds
              this.value = end;
            }

          return this.value;
        },
        setValue: function(value) {
          /*var val = parseInt(value);
          if(val == 'NaN') {
            this.fields.recurEnd.datepicker('setDate', new Date(val));
          }*/
        },
        validate: function() {
        	return [];
        },
        asString: function() {
          return (new Date(this.getValue())).toLocaleString();
        }
      });

    dcComps.device = new ocAdmin.Component([],
      {
        required: false,
        key: 'spatial',
        errors: {
          missingRequired: new ocAdmin.Error('missingAgent', 'recurAgentLabel')
        }
      },
      {
        getValue: function(){
          this.value=ocOptin.selectedCourse.agentName;
          return this.value;
        },
        setValue: function(value) { }
      });

    dcComps.recurrence = new ocAdmin.Component(['scheduleRepeat', 'repeatSun', 'repeatMon', 'repeatTue', 'repeatWed', 'repeatThu', 'repeatFri', 'repeatSat'],
      {
        required: true,
        key: 'recurrence',
        nsPrefix: 'oc',
        nsURI: 'http://www.opencastproject.org/matterhorn/',
        errors: {
          missingRequired: new ocAdmin.Error('errorRecurrence', 'recurrenceLabel')
        }
      },
      {
        getValue: function() {
          var rrule, dotw, days, date, hour, min, dayOffset;
          if(this.validate()) {
        	courseDays = ocOptin.selectedCourse.days;

            rrule     = "FREQ=WEEKLY;BYDAY=";
            dotw      = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'];
            days      = [];
            date      = new Date(ocOptin.components.recurrenceStart.getValue());
            hour      = date.getUTCHours();
            min       = date.getUTCMinutes();
            dayOffset = 0;

            if(date.getDay() != date.getUTCDay()) {
              dayOffset = date.getDay() < date.getUTCDay() ? 1 : -1;
            }
            if(courseDays['SU']) {
              days.push(dotw[(0 + dayOffset) % 7]);
            }
            if(courseDays['MO']) {
              days.push(dotw[(1 + dayOffset) % 7]);
            }
            if(courseDays['TU']) {
              days.push(dotw[(2 + dayOffset) % 7]);
            }
            if(courseDays['WE']) {
              days.push(dotw[(3 + dayOffset) % 7]);
            }
            if(courseDays['TH']) {
              days.push(dotw[(4 + dayOffset) % 7]);
            }
            if(courseDays['FR']) {
              days.push(dotw[(5 + dayOffset) % 7]);
            }
            if(courseDays['SA']) {
              days.push(dotw[(6 + dayOffset) % 7]);
            }
            this.value = rrule + days.toString() + ";BYHOUR=" + hour + ";BYMINUTE=" + min;
          }
          return this.value;
        },
        setValue: function(value) { },
        validate: function() {
          courseDays = ocOptin.selectedCourse.days;

          if( ocOptin.selectedCourse.meeting_days != "       "){
            if(ocOptin.components.recurrenceStart.validate() &&
              ocOptin.components.recurrenceDuration.validate() &&
              ocOptin.components.recurrenceEnd.validate()) {
            return [];
          }
        }

          return this.errors.missingRequired;
        },
        toNode: function(parent) {
          for(var el in this.fields) {
            var container = parent.ownerDocument.createElement(this.nodeKey);
            container.appendChild(parent.ownerDocument.createTextNode(this.getValue()));
          }
          if(parent && parent.nodeType) {
            parent.appendChild(container);
          }
          return container;
        }
      });

    dcComps.temporal = new ocAdmin.Component([],
      {
        key: 'temporal',
        required: true
      },
      {
        getValue: function() {
          var start, end;
          if (this.validate() ) {
        	  start = ocOptin.components.recurrenceStart.getValue();
        	  end = ocOptin.components.recurrenceEnd.getValue();
          };
          return 'start=' + ocUtils.toISODate(new Date(start)) +
            '; end=' + ocUtils.toISODate(new Date(end)) + '; scheme=W3C-DTF;';
        },
        validate: function() {
          var startValid = ocOptin.components.recurrenceStart.validate();
          var durationValid = ocOptin.components.recurrenceDuration.validate();
          var endValid = ocOptin.components.recurrenceEnd.validate();
          var error = [];
          if (startValid.length != 0) {
            error.push(start.concat(startValid));
          }
          if (durationValid.length != 0) {
            error.push(durationValid);
          }
          if (endValid.length != 0) {
            error.push(endValid);
          }
          return error;
        }
      });

    dcComps.agentTimeZone = new ocAdmin.Component([],
      	{
      	  key: 'agentTimeZone',
      	  nsPrefix: 'oc',
      	  nsURI: 'http://www.opencastproject.org/matterhorn/',
      });

    agentComps.workflowDefinition = new ocAdmin.Component(['workflowSelector'], {
          key: 'org.opencastproject.workflow.definition'
        },{
            getValue: function() {  return DEFAULT_WORKFLOW ;},
            setValue: function(value) { }
        });

    agentComps.trimHold = new ocAdmin.Component([], {
    	key: 'org.opencastproject.workflow.config.trimHold'
      },{
    	getValue: function() { return true; }
      });

    agentComps.captionHold = new ocAdmin.Component([], {
        key: 'org.opencastproject.workflow.config.captionHold'
      },{
    	getValue: function() { return true; }
      });

    agentComps.recordingAvailability = new ocAdmin.Component([], {
        key: 'org.opencastproject.workflow.config.recordingAvailability'
      },{
    	getValue: function() {
    	  return $('.recordingAvailability:checked').val(); }
      });

    agentComps.delayPublishByDays = new ocAdmin.Component([], {
        key: 'org.opencastproject.workflow.config.delayPublishByDays'
      },{
    	getValue: function() {
    	  return $('#delayPublishByDaysDays').val(); }
      });

    this.dublinCore.components = dcComps;
    this.components = compositeComps;
    this.capture.components = agentComps;
    if(typeof ocWorkflowPanel != 'undefined') {
      ocWorkflowPanel.registerComponents(ocOptin.capture.components);
    }
  }

  function parseDublinCoreTemporal(temporal) {
    period = temporal.split(' ');
    var start = period[0].slice(period[0].indexOf('=') + 1, -1);
    var end = period[1].slice(period[1].indexOf('=') + 1, -1);
    start = ocUtils.fromUTCDateString(start).getTime();
    end = ocUtils.fromUTCDateString(end).getTime();
    var duration = end - start;
    return {
      start: start,
      end: end,
      dur: duration
    };
  }

  function showUserMessages(errors, type) {
    type = type || 'error';
    if(type === 'error' && $('#missingFieldsContainer').css('display') === 'none') {
      $('#missingFieldsContainer').show();
    } else {
      $('#missingFieldsContainer li').hide();
    }
    for(var i in errors) {
      $('#' + errors[i].name).show();
      for(var j in errors[i].label) {
        var label = errors[i].label[j];
        $('#' + label).addClass('label-error');
      }
    }
  }

  function hideUserMessages() {
    $('#missingFieldsContainer').hide();
    $('#missingFieldsContainer li').hide();
    $('.label-error').removeClass('label-error');
  }

  function getFormattedTime(fourDigitTime) {
    /* make sure add radix*/
    var hours24 = parseInt(fourDigitTime.substring(0, 2),10);
    var hours = ((hours24 + 11) % 12) + 1;
    var amPm = hours24 > 11 ? 'pm' : 'am';
    var minutes = fourDigitTime.substring(2);
    return hours + ':' + minutes + amPm;
  };

  return sched;
}());
