/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.env;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;

import java.util.Dictionary;
import java.util.Properties;

/**
 * @author John Crossman
 */
public abstract class EManagedServiceFactory implements ManagedServiceFactory {

  @Override
  public final synchronized void updated(final String pid, final Dictionary properties) throws ConfigurationException {
    updatedConfiguration(pid, EnvironmentUtil.createEProperties(properties, true));
  }

  protected abstract void updatedConfiguration(String pid, Properties properties) throws ConfigurationException;

}
