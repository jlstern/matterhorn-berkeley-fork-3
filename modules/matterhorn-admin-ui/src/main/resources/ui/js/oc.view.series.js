/**
 *  Copyright 2009 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */

function deCamelCase(camelCaseString) {
  if (camelCaseString == null) {
    return "";
  }
  var result = camelCaseString.replace( /([A-Z])/g, " $1" );
  return result.charAt(0).toUpperCase() + result.slice(1);
}

function renderCourseData(data) {
  term = data['term'];
  canonicalCourse = data['canonicalCourse'];

  $('#stageOfLifecycle').text(deCamelCase(data['stageOfLifecycle']));
  $('#stageOfApproval').text(deCamelCase(data['stageOfApproval']));

  participationData = data['participationSet'];
  participationSet = (participationData.constructor == Array)? participationData : [participationData];
  var instructorArray = [];
  for (i in participationSet) {
    var p = participationSet[i];
    instructorArray.push(p.instructor.firstName + " " + p.instructor.lastName);
  }
  var instructors = instructorArray.join(",")
  $('#contributor').html(instructors);
  $('#participationSet').html(instructors);
  $('#description').html(canonicalCourse['title'] + " (" + term['semester'] + " " + term['termYear'] + ") - " + instructors);

  $('#dates').text(data['meetingDays'].join(",") + "; " + data['startTime'] + " - " + data['endTime']);

  room = data['room'];
  $('#room').text(room['roomNumber'] + " " + room['building']);

  capturePreferences = data['capturePreferences'];
  $('#capture-preferences-recording-type').text(deCamelCase(capturePreferences['recordingType']));
}

var ocViewSeries = (function(){
  var SERIES_URL = '/series';

  var PURL = "http://purl.org/dc/terms/";

  var anonymous_role = "ROLE_ANONYMOUS";

  var trans = {
    read: "View",
    contribute: "Contribute",
    write: "Administer",
    analyze: "Analyze"
  };

  this.initViewSeries = function() {
    $('#addHeader').jqotesubtpl('templates/viewseries.tpl', {});

    var id = ocUtils.getURLParam('seriesId');

    $('.oc-ui-collapsible-widget .ui-widget-header').click( function() {
      $(this).children('.ui-icon').toggleClass('ui-icon-triangle-1-e');
      $(this).children('.ui-icon').toggleClass('ui-icon-triangle-1-s');
      $(this).next().toggle();
      return false;
    });
    $('#id').text(id);
    $.ajax({
      url: "/info/me.json",
      dataType: "json",
      async: false,
      success: function(data)
      {
        anonymous_role = data.org.anonymousRole;
      }
    });
    loadSeries(id);
  };

  this.loadSeries = function(seriesId) {
    var i, mdList, metadata, series;
    if(seriesId !== '') {
      $.get(SERIES_URL + '/' + seriesId + '.json', function(data) {
        $.each(data[PURL], function(key, value)
        {
          $('#' + key).text(value[0].value);
        });
      });

      $.ajax({
        type: 'GET',
        url: '/courses/' + seriesId + '.json',
        dataType: 'json',
        success: function (data) {
          if (data != null) {
            renderCourseData(data);
          }
        },
        error: function () {
          // Do nothing. Not all series IDs will map to course data in Salesforce.
        }
      });

      $.get(SERIES_URL + "/" + seriesId + "/acl.json", function (data)
      {
        var roles = {};
        if(!$.isArray(data.acl.ace)) {
          if(data.acl.ace.role == anonymous_role && data.acl.ace.action == "read" && data.acl.ace.allow == true) {
            roles["Public"] = ["View"];
          }
        } else {
          $.each(data.acl.ace, function(key, value)
          {
            if(!$.isArray(roles[value.role]) && value.role != anonymous_role)
            {
              roles[value.role] = [];
            }
            if(value.action != "contribute" && value.role != anonymous_role)
            {
              roles[value.role].push(trans[value.action]);
            }

            if(value.role == anonymous_role)
            {
              roles["Public"] = ["View"];
            }
          });
        }

        if(roles["Public"] == undefined)
        {
          roles["Public"] = ["No access"];
        }

        $.each(roles, function(key, value)
        {
          roles[key] = value.join(', ');
        });

        $("#privileges-list").jqotesubtpl('templates/viewseries-privileges.tpl', {
          roles: roles
        });
      });
    }
  }

  return this;

})();
