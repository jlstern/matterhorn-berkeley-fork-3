/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.junit.Test;
import org.opencastproject.participation.impl.MVUtils;
import org.opencastproject.participation.model.Instructor;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertEquals;

/**
 * @author John Crossman
 */
public class ResultConverterForInstructorTest extends AbstractResultConverterTest {

  @Test
  public void testConvert() throws SQLException {
    final ResultSet r = createMock(ResultSet.class);
    expectReturn(r, MVUtils.StrCol.ldap_uid);
    expectReturn(r, MVUtils.StrCol.first_name);
    expectReturn(r, MVUtils.StrCol.last_name);
    expectReturn(r, MVUtils.StrCol.email_address);
    expectReturn(r, MVUtils.StrCol.dept_description);
    replay(r);
    //
    final Instructor instructor = new ResultConverterForInstructor().convert(r);
    assertEquals(MVUtils.StrCol.first_name.name(), instructor.getFirstName());
    assertEquals(MVUtils.StrCol.last_name.name(), instructor.getLastName());
    assertEquals(MVUtils.StrCol.email_address.name(), instructor.getEmail());
    assertEquals(MVUtils.StrCol.ldap_uid.name(), instructor.getCalNetUID());
  }

}
