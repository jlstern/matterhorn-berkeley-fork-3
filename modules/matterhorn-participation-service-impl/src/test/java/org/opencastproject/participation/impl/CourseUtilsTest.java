/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.SalesforceFieldInstructor;
import org.opencastproject.util.data.Collections;

import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author John Crossman
 */
public class CourseUtilsTest {

  @Test
  public void testGetDistinctInstructors() {
    final Participation p = new Participation();
    final Instructor instructor = new Instructor();
    instructor.setCalNetUID(new Object().hashCode() + "");
    p.setInstructor(instructor);
    final CourseData c1 = createCourseData(p);
    //
    final Participation[] pArray = new Participation[6];
    for (int index = 0; index < 3; index++) {
      final Participation p1 = new Participation();
      final Instructor i1 = new Instructor();
      i1.setCalNetUID(index + "");
      p1.setInstructor(i1);
      pArray[index] = p1;
      // This next instructor will be a dupe of others created in this loop so they should have no effect on
      // count of distinct instructors
      final Participation p2 = new Participation();
      final Instructor i2 = new Instructor();
      i2.setCalNetUID((index + 1) % 2 + "");
      p2.setInstructor(i2);
      pArray[index + 3] = p2;
    }
    final CourseData c2 = createCourseData(pArray);
    //
    final Set<CourseData> courseDataList = Collections.createSet(c1, c2);
    final Set<Instructor> set = CourseUtils.extractInstructorSet(courseDataList);
    assertEquals(4, set.size());
  }

  @Test
  public void testIsRoomCapable() {
    assertFalse(CourseUtils.isRoomCapable(null));
    assertFalse(CourseUtils.isRoomCapable(new Room()));
    final Room room = new Room();
    room.setCapability(RoomCapability.screencastAndVideo);
    assertTrue(CourseUtils.isRoomCapable(room));
  }

  @Test
  public void testRemoveCoursesWithUnrecognizedRooms() {
    final int arbitraryCCN = this.hashCode();
    final List<Room> allRooms = new LinkedList<Room>();
    final Room evans160 = new Room("salesforceID-1", "Evans", "160", RoomCapability.screencast);
    allRooms.add(evans160);
    final Set<CourseData> courseDataSet = new HashSet<CourseData>();
    final Term term = new Term(Semester.Fall, 2014, null, null);
    courseDataSet.add(createCourseData(term, createCanonicalCourse(arbitraryCCN + 1), evans160));
    final Room unknownRoom = new Room("salesforceID-1", "Evans", "160", null);
    courseDataSet.add(createCourseData(term, createCanonicalCourse(arbitraryCCN + 1), unknownRoom));
    //
    final Set<CourseData> result = CourseUtils.removeCoursesWithUnrecognizedRooms(courseDataSet, allRooms);
    assertEquals(1, result.size());
    final CourseData actual = result.iterator().next();
    assertEquals(evans160, actual.getRoom());
  }

  @Test
  public void testSetSalesforceIDs() {
    final Set<CourseData> courseDataSet = new HashSet<CourseData>();
    final List<Room> allRooms = new LinkedList<Room>();
    final List<Instructor> instructorList = new LinkedList<Instructor>();
    final Term term = new Term(Semester.Fall, 2014, null, null);
    term.setSalesforceID("term-123");
    final int arbitraryCCN = this.hashCode();
    for (int index = 0; index < 3; index++) {
      final Room room = new Room("salesforceID-" + index, "building-" + index, index + 10 + "", RoomCapability.screencast);
      allRooms.add(room);
      final CourseData courseData = new CourseData();
      courseData.setCanonicalCourse(createCanonicalCourse(arbitraryCCN + index));
      courseData.setTerm(new Term(term.getSemester(), term.getTermYear(), null, null));
      courseData.setRoom(clone(room));
      // Arbitrary number of instructors
      final int instructorCount = (index % 2 == 0) ? 1 : 2;
      final long arbitraryID = new Date().getTime();
      for (int instructorIndex = 0; instructorIndex < instructorCount; instructorIndex++) {
        final long nextID = arbitraryID + instructorIndex;
        final Instructor instructor = new Instructor();
        instructor.setCalNetUID("calNetUID-" + nextID);
        instructor.setSalesforceID("salesforce-" + nextID);
        instructorList.add(instructor);
        final Instructor clone = new Instructor();
        clone.setCalNetUID(instructor.getCalNetUID());
        final Participation participation = new Participation(SalesforceFieldInstructor.Instructor_1, clone, false);
        courseData.setParticipationSet(Collections.set(participation));
      }
      courseDataSet.add(courseData);
    }
    CourseUtils.setSalesforceIDs(courseDataSet, allRooms, instructorList, term);
    for (final CourseData courseData : courseDataSet) {
      final String errorMessagePrefix = courseData.getCanonicalCourse().getCcn() + " missing salesforceID on ";
      assertNotNull(errorMessagePrefix + " term", courseData.getTerm().getSalesforceID());
      assertNotNull(errorMessagePrefix + " room " + courseData.getRoom(), courseData.getRoom().getSalesforceID());
      for (final Participation p : courseData.getParticipationSet()) {
        assertNotNull(errorMessagePrefix + " instructor " + p.getInstructor(), p.getInstructor().getSalesforceID());
      }
    }
  }

  private CanonicalCourse createCanonicalCourse(final int ccn) {
    final CanonicalCourse canonicalCourse = new CanonicalCourse();
    canonicalCourse.setCcn(ccn);
    return canonicalCourse;
  }

  private Room clone(final Room room) {
    return new Room(null, room.getBuilding(), room.getRoomNumber(), room.getCapability());
  }

  private CourseData createCourseData(final Participation... participation) {
    final CanonicalCourse cc = new CanonicalCourse();
    final double random = Math.random();
    cc.setCcn(new Double(random * 1000000000).intValue());
    cc.setDepartment("Dept " + random);
    cc.setTitle("Title " + random);
    return createCourseData(new Term(), cc, new Room(), participation);
  }

  private CourseData createCourseData(final Term term, final CanonicalCourse canonicalCourse, Room room, Participation... participation) {
    final CourseData c = new CourseData();
    c.setCanonicalCourse(canonicalCourse);
    c.setTerm(term);
    c.setRoom(room);
    c.setParticipationSet(Collections.createSet(participation));
    return c;
  }

}
