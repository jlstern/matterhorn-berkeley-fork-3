/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertTrue;

import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.persistence.ProvidesCourseCatalogData;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.util.NotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author John Crossman
 */
public class CourseDataMoverMockTest {

  private CourseDataMover dataMover;
  private CourseManagementService courseManagementService;
  private ProvidesCourseCatalogData courseDatabase;
  private VoidNotifier notifier;
  private final Term term = new Term(Semester.Fall, 2013, null, null);

  @Before
  public void before() {
    courseManagementService = createMock(CourseManagementService.class);
    final Set<Term> allTermSet = new HashSet<Term>();
    allTermSet.add(term);
    expect(courseManagementService.getAllTerms()).andReturn(allTermSet).anyTimes();
    courseDatabase = createMock(ProvidesCourseCatalogData.class);
    //
    dataMover = new CourseDataMover();
    dataMover.setCourseManagementService(courseManagementService);
    dataMover.setCourseDatabase(courseDatabase);
    notifier = new VoidNotifier();
    dataMover.setNotifier(notifier);
  }

  @Test
  public void testNoCaptureEnabledRooms() throws NotFoundException {
    final Set<Room> rooms = getRoomSet();
    rooms.add(new Room("salesforceID-1", "Dwinelle", "8", null));
    rooms.add(new Room("salesforceID-2", "Wheeler", "80", null));
    rooms.add(new Room("salesforceID-3", "Evans", "800", null));
    expect(courseManagementService.getAllRooms()).andReturn(rooms).once();
    // No rooms are capture enabled so we tell mock objects to expect NO call to courseDatabase
    //
    final Set<CourseData> set = createCourseDataSet(rooms);
    expect(courseDatabase.getCourseData(term.getSemester(), term.getTermYear())).andReturn(set).once();
    replay(courseManagementService, courseDatabase);
    dataMover.run();
  }

  @Test
  public void testUpsertCourses() throws NotFoundException {
    final Set<Room> rooms = getRoomSet(RoomCapability.screencastAndVideo, RoomCapability.screencast);
    //
    final Set<CourseData> set = createCourseDataSet(rooms);
    expect(courseDatabase.getCourseData(term.getSemester(), term.getTermYear())).andReturn(set).once();
    //
    expect(courseManagementService.getAllRooms()).andReturn(rooms).once();
    final List<Instructor> result = createInstructorList(set);
    expect(courseManagementService.createOrUpdateInstructors(anyObject(Set.class))).andReturn(result).once();
    courseManagementService.createOrUpdateCourses(anyObject(Set.class));
    expectLastCall().once();
    replay(courseManagementService, courseDatabase);
    dataMover.run();
  }

  private List<Instructor> createInstructorList(final Set<CourseData> set) {
    final List<Instructor> instructorSet = new LinkedList<Instructor>();
    for (final CourseData courseData : set) {
      final Instructor instructor = new Instructor();
      final long time = new Date().getTime();
      instructor.setCalNetUID("calNetUID-" + time);
      if (courseData.hashCode() % 2 == 0) {
        instructor.setCalNetUID("calNetUID-" + (time + 1));
      }
      instructorSet.add(instructor);
    }
    return instructorSet;
  }

  @After
  public void after() {
    assertTrue(notifier.isEngineeringNotified());
  }

  private Set<CourseData> createCourseDataSet(final Set<Room> roomSet) {
    final Set<CourseData> courseOfferings = new HashSet<CourseData>();
    for (final Room room : roomSet) {
      final CourseOffering courseOffering = new CourseOffering();
      final CanonicalCourse canonicalCourse = new CanonicalCourse();
      canonicalCourse.setCcn(room.hashCode());
      courseOffering.setCanonicalCourse(canonicalCourse);
      courseOffering.setRoom(room);
      courseOfferings.add(courseOffering);
    }
    return courseOfferings;
  }

  private Set<Room> getRoomSet(final RoomCapability... capabilityList) {
    final Set<Room> roomSet = new HashSet<Room>();
    for (final RoomCapability capability : capabilityList) {
      final Room room = new Room();
      room.setBuilding("building");
      room.setRoomNumber(new Object().hashCode() + "");
      room.setCapability(capability);
      roomSet.add(room);
    }
    return roomSet;
  }

}
