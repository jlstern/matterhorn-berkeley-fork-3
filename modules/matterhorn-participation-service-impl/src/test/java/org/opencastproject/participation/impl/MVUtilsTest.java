/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.opencastproject.participation.model.DayOfWeek;

import org.junit.Test;

import java.util.List;

/**
 * @author John Crossman
 */
public final class MVUtilsTest {

  @Test
  public void testGetMeetingDays() {
    assertSameDaysOfWeek(MVUtils.getMeetingDays("   W  "), DayOfWeek.Wednesday);
    assertSameDaysOfWeek(MVUtils.getMeetingDays("  T T "), DayOfWeek.Tuesday, DayOfWeek.Thursday);
    assertSameDaysOfWeek(MVUtils.getMeetingDays(" mTwTf"), DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday);
    assertNull(MVUtils.getMeetingDays("      "));
    assertNull(MVUtils.getMeetingDays("UNSCHEDULED"));
  }

  @Test
  public void testGetByIndexTooLong() {
    // We don't support Sat and Sun references
    assertSameDaysOfWeek(MVUtils.getMeetingDays(" MTWTF S"), DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetByIndexInvalidCharPlacement() {
    MVUtils.getMeetingDays("  W T ");
  }

  private void assertSameDaysOfWeek(final List<DayOfWeek> actualList, final DayOfWeek... expectedList) {
    assertEquals(expectedList.length, actualList.size());
    int index = 0;
    for (final DayOfWeek expected : expectedList) {
      assertSame(expected, actualList.get(index++));
    }
  }

}
