/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation;

import org.opencastproject.participation.impl.persistence.DatabaseCredentials;
import org.osgi.service.cm.ConfigurationException;

import java.util.Properties;

/**
 * @author John Crossman
 */
public class UnitTestUtils {

  public static Properties getSalesforceConnectionProperties() throws ConfigurationException {
    final Properties p = new Properties();
    p.setProperty("salesforce.connector.setConnectionTimeout", "60000");
    p.setProperty("salesforce.connector.username", "matterhorn@lists.berkeley.edu.GPdev");
    // Security token, "PPptvGndmbCIZe9lJd00XdHT", is not needed
    p.setProperty("salesforce.connector.setPassword", "gr!zzlyp3@kPPptvGndmbCIZe9lJd00XdHT");
    p.setProperty("salesforce.connector.setAuthEndpoint", "https://test.salesforce.com/services/Soap/u/22.0");
    return p;
  }

  public static DatabaseCredentials getCourseDatabaseCredentials() {
    return new DatabaseCredentials("dba-oracle-dev-10.ist.berkeley.edu", 1523, "bspdev", "ETS_WEBCAST", "lhpu$1k4");
  }
}
