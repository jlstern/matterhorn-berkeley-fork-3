/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author John Crossman
 */
public class InstructorTest {


  @Test
  public void testEquals() {
    final Instructor joe = new Instructor("132ABC ");
    joe.setDepartment("Foo");
    joe.setSalesforceID("Joe");
    joe.setLastName("Taylor-Swift");
    joe.setEmail("joe@media.berkeley.edu");
    //
    final Instructor joey = new Instructor("132ABC ");
    joey.setDepartment("Baz");
    joey.setSalesforceID("Joey");
    joey.setLastName("Taylor");
    joey.setEmail("joe@berkeley.edu");
    //
    assertEquals(joe, joey);
    //
    final Map<Instructor, String> map = new HashMap<Instructor, String>();
    map.put(new Instructor("999AAA"), "value1");
    map.put(joey, "value2");
    map.put(new Instructor("111ZZZ"), "value3");
    map.put(new Instructor("555MMM"), "value4");
    assertTrue(map.containsKey(joe));
  }

}
