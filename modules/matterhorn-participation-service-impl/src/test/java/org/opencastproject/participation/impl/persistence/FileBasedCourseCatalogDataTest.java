/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;

import org.junit.Test;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * @author John Crossman
 */
@Ignore
public class FileBasedCourseCatalogDataTest {

  private final FileBasedCourseCatalogData catalogData;

  public FileBasedCourseCatalogDataTest() throws IOException {
    final Resource resource = new FileSystemResource("/courseCatalogDataTest/courses/modern-physics-1.xml");
    final File coursesDirectory = new File(resource.getFile().getParent());
    catalogData = new FileBasedCourseCatalogData(coursesDirectory.getParent());
  }

  @Test
  public void testNoMatchingPerRoom() {
    final Set<CourseData> courseDataSet = catalogData.getCourseData(Semester.Spring, 2013);
    assertEquals(2, courseDataSet.size());
  }

  @Test
  public void testNoMatchingPerTerm() {
    final Set<CourseData> courseDataSet = catalogData.getCourseData(Semester.Fall, 2013);
    assertEquals(1, courseDataSet.size());
  }

  @Test
  public void testAllMatching() {
    final Set<CourseData> courseDataSet = catalogData.getCourseData(Semester.Spring, 2013);
    assertEquals(2, courseDataSet.size());
    for (final CourseData courseData : courseDataSet) {
      assertCorrectInstructors(courseData);
      assertTrue(courseData.getCanonicalCourse().getCcn() != null);
      assertEquals("Physics department", courseData.getCanonicalCourse().getDepartment());
      assertEquals("Modern Physics", courseData.getCanonicalCourse().getTitle());
      final Term term = courseData.getTerm();
      assertNotNull(term.getSemesterStartDate());
      assertNotNull(term.getSemesterEndDate());
      assertNotNull(term.getSemester());
      assertNotNull(term.getTermYear());
      assertNotNull(courseData.getStartTime());
      assertNotNull(courseData.getEndTime());
      assertEquals("Dwinelle", courseData.getRoom().getBuilding());
      assertNotNull(courseData.getRoom().getRoomNumber());
      final List<DayOfWeek> meetingDays = courseData.getMeetingDays();
      assertEquals(3, meetingDays.size());
      assertTrue(meetingDays.contains(DayOfWeek.Monday));
      assertTrue(meetingDays.contains(DayOfWeek.Wednesday));
      assertTrue(meetingDays.contains(DayOfWeek.Friday));
      assertNotNull(courseData.getSection());
      assertNotNull(courseData.getStudentCount());
    }
  }

  private void assertCorrectInstructors(final CourseData courseData) {
    final CanonicalCourse canonicalCourse = courseData.getCanonicalCourse();
    final Set<Participation> set = courseData.getParticipationSet();
    switch (canonicalCourse.getCcn()) {
      case 1:
        assertEquals(3, set.size());
        assertJudy(set);
        break;
      case 2:
        assertEquals(2, set.size());
        assertJudy(set);
        assertViolet(set);
        break;
      case 3:
        assertEquals(3, set.size());
        assertJudy(set);
        assertViolet(set);
        assertDoralee(set);
        break;
      default:
        fail("Unexpected CCN (" + canonicalCourse.getCcn() + ") for course " + canonicalCourse.getTitle());
    }
  }

  private void assertJudy(final Set<Participation> set) {
    assertInstructor(set, "Judy", "Bernly", "judith@berkeley.edu");
  }

  private void assertViolet(final Set<Participation> set) {
    assertInstructor(set, "Violet", "Newstead", "violetnewstead@berkeley.edu");
  }

  private void assertDoralee(final Set<Participation> set) {
    assertInstructor(set, "Doralee", "Rhodes", "doralee@berkeley.edu");
  }

  private void assertInstructor(final Set<Participation> set, final String firstName, final String lastName, final String emailAddress) {
    boolean foundMatch = false;
    for (final Participation participation : set) {
      final Instructor instructor = participation.getInstructor();
      if (instructor.getEmail().equals(emailAddress)) {
        assertEquals(firstName, instructor.getFirstName());
        assertEquals(lastName, instructor.getLastName());
        foundMatch = true;
        break;
      }
    }
    if (!foundMatch) {
      fail("Instructor not found: " + firstName + " " + lastName + " " + emailAddress);
    }
  }

}
