/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.junit.runners;

import org.junit.internal.runners.model.EachTestNotifier;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

/**
 * If you want to designate a suite of unit tests that only run once per day then follow these steps:
 * <ol>
 *   <li>Annotate your unit test class using {@link org.junit.runner.RunWith(Class)}, passing {@link SysTestJUnit4ClassRunner#getClass()}</li>
 *   <li>Configure your build server (e.g., Bamboo) to: (a) run once per day and (b) pass JVM arg "-DrunSysTests=true" to the build</li>
 * </ol>
 * @author John Crossman
 */
public class SysTestJUnit4ClassRunner extends BlockJUnit4ClassRunner {

  private static final String JVM_ARG_NAME = "runSysTests";

  public SysTestJUnit4ClassRunner(final Class<?> klass) throws InitializationError {
    super(klass);
  }

  @Override
  protected void runChild(final FrameworkMethod method, final RunNotifier notifier) {
    final boolean runSysTest = "true".equalsIgnoreCase(System.getProperty(JVM_ARG_NAME));
    if (runSysTest) {
      super.runChild(method, notifier);
    } else {
      final EachTestNotifier eachNotifier = new EachTestNotifier(notifier, describeChild(method));
      eachNotifier.fireTestIgnored();
    }
  }

}
