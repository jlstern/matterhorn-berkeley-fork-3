/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.util.env.EnvironmentUtil;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * @author John Crossman
 */
public class ViewProperties {

  private final Properties properties;

  public ViewProperties(final String viewName) throws IOException {
    properties = new Properties();
    final File xmlFile = EnvironmentUtil.getFileUnderFelixHome("shared-resources/messages-" + viewName + ".xml");
    if (xmlFile.exists()) {
      properties.loadFromXML(FileUtils.openInputStream(xmlFile));
    }
  }

  public String get(final Object key, final Object... args) {
    final String keyString = key.toString();
    final String property = StringUtils.trimToNull(properties.getProperty(keyString));
    String value = property == null ? keyString : property;
    int counter = 0;
    for (final Object arg : args) {
      value = value.replace("{" + counter++ + "}", arg.toString());
    }
    return value;
  }

}
