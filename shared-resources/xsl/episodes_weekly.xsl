<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ns2="http://search.opencastproject.org" xmlns:mp="http://mediapackage.opencastproject.org">
	<xsl:param name="series"></xsl:param>
	<xsl:template match="/*">
		<xsl:result-document href="#stage" method="replace-content"><div>
		<xsl:variable name="first_week" as="xs:double">
			<xsl:for-each select="ns2:result">
				<xsl:sort select="ns2:dcCreated" order="ascending"/>
				<xsl:if test="position()=1">
					<xsl:sequence select="floor(number(format-date(xs:date(substring(ns2:dcCreated,1,10)),'[W]'))) - 1"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		
		<xsl:if test="$series != ''">
		<nav>
		Week 
		<xsl:for-each-group select="ns2:result" group-by="format-date(xs:date(substring(ns2:dcCreated,1,10)),'[W]')">
			<xsl:sort select="ns2:dcCreated" order="ascending"/>
			<a><xsl:attribute name="href">#weekgroup-<xsl:value-of select="number(current-grouping-key()) - $first_week"/></xsl:attribute><xsl:value-of select="number(current-grouping-key()) - $first_week"/></a>
		</xsl:for-each-group>				
		</nav>
		</xsl:if>
		
		<xsl:for-each-group select="ns2:result" group-by="format-date(xs:date(substring(ns2:dcCreated,1,10)),'[W]')">
			<xsl:sort select="ns2:dcCreated" order="descending"/>
			<section class="weekgroup"><a><xsl:attribute name="id">weekgroup-<xsl:value-of select="number(current-grouping-key()) - $first_week"/></xsl:attribute></a>
				<header class="weekheader"><h2>
				<xsl:if test="$series != ''">Week <xsl:value-of select="number(current-grouping-key()) - $first_week"/>
				</xsl:if>
				</h2></header>
				<div class="lecgroup">
					
				<xsl:for-each select="current-group()">
					<article>
						<header>
							<a><xsl:attribute name="href">watch.html?id=<xsl:value-of
								select="mp:mediapackage/@id" /></xsl:attribute>
							<img class="thumb" alt="">
								<xsl:for-each select="mp:mediapackage/mp:attachments/mp:attachment">
									<xsl:choose>
										<xsl:when test="@type='presenter/search+preview'">
											<xsl:attribute name="src"><xsl:value-of select="mp:url" /></xsl:attribute>
										</xsl:when>
										<xsl:when test="@type='presentation/search+preview'">
											<xsl:attribute name="src"><xsl:value-of select="mp:url" /></xsl:attribute>
										</xsl:when>
									</xsl:choose>
								</xsl:for-each>
							</img></a>
							
							
							<h3><a><xsl:attribute name="href">watch.html?id=<xsl:value-of
										select="mp:mediapackage/@id" /></xsl:attribute>
										<xsl:attribute name="title"><xsl:value-of select='ns2:dcTitle'/></xsl:attribute>
										<xsl:value-of select='substring(ns2:dcTitle, 0, 60)' />
										<xsl:if test='string-length(ns2:dcTitle)>60'>
											...
										</xsl:if></a></h3>
							<p class="recdate">
							  <time>
								<xsl:attribute name="datetime">
									<xsl:value-of select="format-date(xs:date(substring(ns2:dcCreated,1,10)),'[Y]-[M]-[D]')"/></xsl:attribute>
								<xsl:value-of select="format-date(xs:date(substring(ns2:dcCreated,1,10)),'[F]')"/><br/>
								<xsl:value-of select="format-date(xs:date(substring(ns2:dcCreated,1,10)),'[MNn] [Do], [Y]')"/>
							  </time>
							</p>
						</header>
						<p class="presenter">
						<xsl:if test="ns2:dcCreator!=''"><xsl:value-of select="ns2:dcCreator" />
						</xsl:if>
						</p>
					</article>
										
				</xsl:for-each>
				</div>
			
			</section>
		</xsl:for-each-group>
		</div>
		</xsl:result-document>
	</xsl:template>
</xsl:stylesheet>
